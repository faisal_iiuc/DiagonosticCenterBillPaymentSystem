﻿using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class TypeWiseReportManager
    {
        TypeWiseReportGateway typeWiseReportGateway = new TypeWiseReportGateway();
        TestTypeManager testTypeManager = new TestTypeManager();
        

        public List<TypeWiseReportView> SearchTypeWiseReport(DateRange dateRange)
        {

            List<TypeWiseReportView> typeWiseReportViews = new List<TypeWiseReportView>();
            List<TestType> testTypes = new List<TestType>();

            testTypes = testTypeManager.GetAllTestTypes();


            foreach (var type in testTypes)
            {

                TypeWiseReportView typeWiseReportView = typeWiseReportGateway.SearchTypeWiseReport(type, dateRange);
                if (typeWiseReportView.TotalNoOfTest != 0)
                {
                    typeWiseReportViews.Add(typeWiseReportView);

                }
                else
                {
                    typeWiseReportView.TotalAmount = 0;
                    typeWiseReportView.TotalNoOfTest = 0;
                    typeWiseReportViews.Add(typeWiseReportView);
                }
            }


            return typeWiseReportViews;
        }

    }
}
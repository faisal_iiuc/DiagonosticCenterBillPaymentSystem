﻿using DiagonosticCenterBillPaymentSystem.DAL;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class RequestManager
    {
        RequestGateway requestGateway = new RequestGateway();


        public bool SaveRequest(List<PatientTestRequest> requests, int patientId)
        {
            bool a = false;
            foreach (var request in requests)
            {
                request.PatientId = patientId;
                request.PaymentStatus = false;

                a = requestGateway.SaveRequest(request);
            }
            return a;
        }
    }
}
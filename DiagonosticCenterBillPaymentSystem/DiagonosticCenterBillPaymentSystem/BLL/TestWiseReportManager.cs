﻿using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class TestWiseReportManager
    {
        TestSetupManager aTestSetupManager = new TestSetupManager();
        TestWiseReportGateway aTestWiseReportGateway = new TestWiseReportGateway();
        public List<TestWiseReportView> SearchTestWiseReport(DateRange dateRange)
        {

            List<TestWiseReportView> testWiseReportViews = new List<TestWiseReportView>();
            List<Test> tests = new List<Test>();

            tests = aTestSetupManager.GetAllTests();


            foreach (var type in tests)
            {
                TestWiseReportView testWiseReportView = aTestWiseReportGateway.SearchTestWiseReport(type, dateRange);
                if (testWiseReportView.TotalNoOfTest != 0)
                {
                    testWiseReportViews.Add(testWiseReportView);
                }

                else
                {
                    testWiseReportView.TotalAmount = 0;
                    testWiseReportView.TotalNoOfTest = 0;
                    testWiseReportViews.Add(testWiseReportView);
                }

            }


            return testWiseReportViews;




        }

       

    }
}
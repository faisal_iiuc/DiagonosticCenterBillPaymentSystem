﻿using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class UnpaidBillManager
    {
        UnpaidBillGateway aUnpaidBillGateway = new UnpaidBillGateway();
        public List<UnpaidBillView> SearchUnpaidBill(DateRange date)
        {
            return aUnpaidBillGateway.SearchUnpaidBill(date);


        }

        public List<UnpaidBillView> SearchAllUnpaidBill()
        {
            return aUnpaidBillGateway.SearchAllUnpaidBill();
        }
    }
}
﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
   
    public class PatientManager
    {
        PatientGateway aPatientGateway = new PatientGateway();

        public Patient SavePatient(Patient patient)
        {
            patient.PaymentStatus = 0;
            patient.DueDate = DateTime.Now.ToString();

            string serialNo = aPatientGateway.SavePatient(patient);
            
                patient= SearchPatientUsingBillNo(serialNo);
                patient.BillNo = serialNo;
                 return patient;
           
            

                 
            
           
        }

        public Patient UpdatePatient(Patient patient)
        {
            return aPatientGateway.UpdatePatient(patient);
        }

        

        public Patient SearchPatientUsingMobile(string p)
        {
            return aPatientGateway.SearchPatientUsingMobile(p);
        }

        public Patient SearchPatientUsingBillNo(string serialNo)
        {

            Patient aPatient = aPatientGateway.SearchPatientUsingSerialNo(serialNo);
            return aPatient;
       
        }





        public string ClearDue(Patient patient)
        {
            
           if (UpdatePatient(patient)==null)
           {
               return "Not Paid";
           }
            else
           {
               return "Payment Sucessfull!!";
           }
        }
        public double IsPatientPaymentClear(string mobileNo)
        {
            Patient patient = SearchPatientUsingMobile(mobileNo);
            if (patient != null)
            {
                return patient.Payment;
            }
            return 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;


namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class TestSetupManager
    {
        TestSetupGateway aTestSetupGateway = new TestSetupGateway();

        private int rowAffected;

        public string Save(Test testSetup)
        {
            if (aTestSetupGateway.DoesTestNameExist(testSetup))
            {
                return "Test Name already exists";
            }
            else
            {
                rowAffected = aTestSetupGateway.Save(testSetup);
                if (rowAffected > 0)
                {
                    return "Saved Successfully";
                }
                else
                {
                    return "Save Failed";
                }
            }
        }

        public List<TestSetupVM> GetTestSetupWithTestNameType()
        {
            return aTestSetupGateway.GetTestSetupWithTestNameType();
        }



        public double GetFee(Test test)
        {
           return aTestSetupGateway.GetFee(test);
        }

        public List<Test> GetAllTests()
        {
            List<Test> aTest = aTestSetupGateway.GetAllTest();
            return aTest;
        }


    }
}
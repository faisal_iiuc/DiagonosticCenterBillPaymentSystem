﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;

namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class TestTypeManager
    {
        private int rowAffected;
        TestTypeGateway aTestTypeGateway = new TestTypeGateway();
        public string Save(TestType testType)
        {
            if (aTestTypeGateway.DoesTestNameExist(testType))
            {
                return "Test name already exists";
            }
            else
            {
                rowAffected = aTestTypeGateway.Save(testType);
                if (rowAffected > 0)
                {
                    return "Saved Successfully";
                }
                else
                {
                    return "Save Failed";
                }
            }
        }

        public List<TestType> GetAllTestTypes()
        {
            return aTestTypeGateway.GetAllTestTypes();
        }
    }
}

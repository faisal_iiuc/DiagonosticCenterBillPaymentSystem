﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
namespace DiagonosticCenterBillPaymentSystem.BLL
{
    public class SystemManager
    {
        GatewayManager aGatewayManager = new GatewayManager();

        
        public  List<UnpaidBillView> SearchUnpaidBill(DateRange date)
        {
            return aGatewayManager.SearchUnpaidBill(date);
 

        }

        public List<TestType> SearchAllType()
        {
            List<TestType> testTypes = new List<TestType>();

            return testTypes = aGatewayManager.SearchType();
        }

        public List<TypeWiseReportView> SearchTypeWiseReport(DateRange dateRange)
        {

            List<TypeWiseReportView> typeWiseReportViews = new List<TypeWiseReportView>();
             List<TestType> testTypes = new List<TestType>();

            testTypes = aGatewayManager.SearchType();


            foreach (var type in testTypes)
            {
                
                TypeWiseReportView typeWiseReportView = aGatewayManager.SearchTypeWiseReport(type,dateRange);
                if(typeWiseReportView.TotalNoOfTest!=0)
                {
                    typeWiseReportViews.Add(typeWiseReportView);
           
                }
                 }


            return typeWiseReportViews;
            
            


        }
        public List<TestWiseReportView> SearchTestWiseReport(DateRange dateRange)
        {

            List<TestWiseReportView> testWiseReportViews = new List<TestWiseReportView>();
            List<Test> test = new List<Test>();

            test = aGatewayManager.SearchTest();


            foreach (var type in test)
            {
               TestWiseReportView testWiseReportView = aGatewayManager.SearchTestWiseReport(type, dateRange);
                if(testWiseReportView.TotalNoOfTest!=0)
                {
                    testWiseReportViews.Add(testWiseReportView);
                }
                
            }


            return testWiseReportViews;




        }

        public PaymentView SearchPatientUsingMobile(string mobileNo)
        {
            return aGatewayManager.SearchPatientUsingMobile(mobileNo);
        }


        public PaymentView SearchPatientUsingSerialNo(string serialNo)
        {
            return aGatewayManager.SearchPatientUsingSerialNo(serialNo);
        }



        public bool SaveRequest(List<Request> requests,Patient patient)
        {
            foreach (var amountCalculate in requests)
            {
                patient.Payment += amountCalculate.TestFee;
            }
            patient.PaymentStatus = 0;
            patient.DueDate = DateTime.Now.ToString();



            patient.PatientId=aGatewayManager.FindExistMobileNo(patient.MoblieNo);

            if (patient.PatientId != 0)
            {

                return aGatewayManager.UpdatePatient(patient);


            }
            else if (aGatewayManager.SavePatient(patient))
            {

                patient.PatientId = aGatewayManager.FindExistMobileNo(patient.MoblieNo);
            }
            else
            {
                return false;
            }
            return SaveRequest(patient,requests);
            




        }

        private bool SaveRequest(Patient patient, List<Request> requests)
        {
            bool a = false;
            foreach (var request in requests)
            {
                request.PatientId = patient.PatientId;

                a = aGatewayManager.SaveRequest(request);
            }
            return a;
        }

        public List<Test> GetAllTests()
        {
            List<Test> aTest = aGatewayManager.SearchTest();
            return aTest;
        }
    }
}
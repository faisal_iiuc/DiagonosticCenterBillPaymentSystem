﻿using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    
    public partial class Payment : System.Web.UI.Page
    {
        Patient patient = new Patient();
        PatientManager patientManager = new PatientManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            billPayButton.Enabled = false;
        }

        protected void searchPatientButton_Click(object sender, EventArgs e)
        {
           
                patient.BillNo = searchBillNoTextBox.Text.ToString();

                patient = patientManager.SearchPatientUsingBillNo(patient.BillNo);
          
          

            if(patient.Payment==0)
            {
                messageLabel.Text = "Patient Not Found";
                billPayButton.Enabled = false;
                return;
            }
            if(patient.PaymentStatus==1)
            {
                messageLabel.Text = "Bill Has been Paid!!";
               
            }
            else
            {
                messageLabel.Text = "Bill not Paid!!";
                billPayButton.Enabled = true;
            }
            payAmountTextBox.Text = patient.Payment.ToString();
            payDueDateTextBox.Text = patient.DueDate;
            
        }

        protected void searchMobNoTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void searchBillNoTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void billPayButton_Click(object sender, EventArgs e)
        {
           

                patient.BillNo = searchBillNoTextBox.Text.ToString();

                messageLabel.Text=patientManager.ClearDue(patient);
                
                
       
           
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.Payment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="font-weight: 700">
        <fieldset style="width: 35%">
            <legend> <strong>Payment</strong></legend>
            <br/>
            
            <center> 
            <asp:Panel ID="Panel1" runat="server" Height="314px" Width="477px">
                
                
                <fieldset style="width: 90%" >
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Bill No" style="font-weight: 700"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="searchBillNoTextBox" runat="server" OnTextChanged="searchBillNoTextBox_TextChanged"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="searchPatientButton" runat="server" Text="Search" OnClick="searchPatientButton_Click"></asp:Button>
                        </td>
                    </tr>
                    
                   
                </table>
                    
                    </fieldset>
                
                
                <fieldset style="width: 90%">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Amount"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="payAmountTextBox" runat="server"></asp:TextBox>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Due Date"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="payDueDateTextBox" runat="server"></asp:TextBox>
                        </td>
                         <td>
                            <asp:Button ID="billPayButton" runat="server" Text="Pay" Width="70px" OnClick="billPayButton_Click"></asp:Button>
                             <br />
                        </td>
                    </tr>
                </table>
                    
                    </fieldset><br /> &nbsp;<asp:Label ID="messageLabel" runat="server"></asp:Label>
                
            </asp:Panel>
            </center>

        </fieldset>

        

    </div>
</asp:Content>

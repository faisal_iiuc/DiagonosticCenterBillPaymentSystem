﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="TestTypeSetUp.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.TestTypeSetUp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 209px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <fieldset style="width: 35%">
            <legend>Test Type Setup</legend>
            <asp:Panel ID="Panel1" runat="server" BorderStyle="Outset" BorderWidth="1px">
                <table >
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Type Name"></asp:Label>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="typeNameTextBox" runat="server" Width="189px"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="saveTypeButton" runat="server" Text="Save" Height="22px" Width="68px" OnClick="saveTypeButton_Click" />
                            <br />
                                                   </td>
                    </tr>
                </table>
                <center><asp:Label ID="messageLabel" runat="server"></asp:Label></center> 

                <br />
                <asp:GridView ID="testTypeGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="494px" Font-Names="Comic Sans MS">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField HeaderText="SL.NO">
                   <ItemTemplate>
                     <%#Container.DataItemIndex+1 %>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TypeName" HeaderText="Type Name" SortExpression="TypeName" />
            </Columns>
            <EditRowStyle BackColor="#999999" HorizontalAlign="Center" VerticalAlign="Middle" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    >
                <br />
                <br />
                <br />
                <br />
            </asp:Panel>
        </fieldset>
    </div>
</asp:Content>

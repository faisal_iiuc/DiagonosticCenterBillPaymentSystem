﻿using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class TestWiseReport : System.Web.UI.Page
    {
        TestWiseReportManager testWiseReportManager = new TestWiseReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            testWisePdfGenButton.Enabled = false;
        }

        protected void typeWiseShowButton_Click(object sender, EventArgs e)
        {
            DateRange dateRange = new DateRange();
            dateRange.Date1 = date1SearchTextBox.Text;
            dateRange.Date2 = date2SearchTextBox.Text;



            List<TestWiseReportView> testWiseReportViews = testWiseReportManager.SearchTestWiseReport(dateRange);

            double total=0;
            foreach (var calculateTotal in testWiseReportViews)
            {
                total += calculateTotal.TotalAmount;
            }

            testWiseTotalTextBox.Text = total.ToString();
            PopulatedGridView(testWiseReportViews);
            ViewState["TestWiseReportView"] = testWiseReportViews;
            
        }

        private void PopulatedGridView(List<TestWiseReportView> testWiseReportViews)
        {
            testWiseReportGridView.DataSource=testWiseReportViews;
            testWiseReportGridView.DataBind();
            testWisePdfGenButton.Enabled = true;
        }

        protected void testWisePdfGenButton_Click(object sender, EventArgs e)
        {
            List<TestWiseReportView> testWiseReportViews = (List<TestWiseReportView>)ViewState["TestWiseReportView"];
            pdfGenerate(testWiseReportViews);
        }

        private void pdfGenerate(List<TestWiseReportView> testWiseReportViews)
        {
            using (StringWriter sW = new StringWriter())
            {
                using (HtmlTextWriter hW = new HtmlTextWriter(sW))
                {
                    StringBuilder sB = new StringBuilder();

                    sB.Append("<h2 align='left'  >Date:" + DateTime.Now + "</h2><br>");
                    sB.Append("<h2 align='left'  >K.F.S.S Diagonostic Center</h2><br>");
                    sB.Append("<h2 align='left'  >TestWiseReport</h2><br><br><br>");
                    sB.Append("<Table align='Center' Border='1'><tr><th>Sl.No</th><th>TestName</th><th>No Of Test</th><th>Total Amount</th></tr>");
                    double total = 0;
                    int index = 0;
                    foreach (var testWiseReport in testWiseReportViews)
                    {
                        index += 1;
                        total += testWiseReport.TotalAmount;
                        sB.Append("<tr><td>");
                        sB.Append(index);
                        sB.Append("</td>");
                        sB.Append("<td>");
                        sB.Append(testWiseReport.TestName);
                        sB.Append("</td><td>");
                        sB.Append(testWiseReport.TotalNoOfTest);
                        sB.Append("</td><td>");
                        sB.Append(testWiseReport.TotalAmount);
                        sB.Append("</td></tr>");

                    }

                    sB.Append("</Table></br>");
                    sB.Append("Total:");
                    sB.Append(total);

                    StringReader sR = new StringReader(sB.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlParser.Parse(sR);
                    pdfDoc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment;filename=TestWiseReport_" + DateTime.Now + ".pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();


                }


            }

        }

    }
}
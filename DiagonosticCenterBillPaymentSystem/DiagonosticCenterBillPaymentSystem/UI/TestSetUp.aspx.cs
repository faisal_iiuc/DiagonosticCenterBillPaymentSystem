﻿using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiagonosticCenterBillPaymentSystem.UI;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class TestSetUp : System.Web.UI.Page
    {
        TestSetupManager aTestSetupManager = new TestSetupManager();
        TestTypeManager aTestTypeManager = new TestTypeManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<TestType> testTypes = aTestTypeManager.GetAllTestTypes();

                testTypeDropDownList.DataSource = testTypes;
                testTypeDropDownList.DataTextField = "TypeName";
                testTypeDropDownList.DataValueField = "TypeId";

                testTypeDropDownList.DataBind();
            }
            PopulateDataGridView();
        }

        private void PopulateDataGridView()
        {
            List<TestSetupVM> aTestSetupVM = aTestSetupManager.GetTestSetupWithTestNameType();
            testSetupGridView.DataSource = aTestSetupVM;
            testSetupGridView.DataBind();

        }
        protected void saveTestSetupButton_Click(object sender, EventArgs e)
        {
            Test aTestSetup = new Test();

            aTestSetup.TestName = testNameTextBox.Text;
            aTestSetup.TestFee = Convert.ToDouble(testFeeTextBox.Text);
            aTestSetup.TypeId = Convert.ToInt32(testTypeDropDownList.SelectedValue);
           

            messageLabel.Text = aTestSetupManager.Save(aTestSetup);

            testNameTextBox.Text = "";
            testFeeTextBox.Text = "";

            PopulateDataGridView();

        }

    }
}
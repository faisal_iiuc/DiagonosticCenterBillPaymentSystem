﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="UnpaidBillReport.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.UnpaidBillReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <fieldset style="width: 35%">
            <legend>Unpaid Bill Report</legend>
            <br/>
            <center> 
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="From Date" style="font-weight: 700"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="date1TextBox" runat="server" Width="102px"></asp:TextBox>
                        </td>
                         
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="To Date" style="font-weight: 700"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="date2TextBox" runat="server" Width="106px"></asp:TextBox>
                        </td>
                         <td>
                            &nbsp;
                            <asp:Button ID="typeWiseShowButton" runat="server" Text="Show" Width="54px" OnClick="typeWiseShowButton_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
             
            </center>
        </fieldset>
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="messageLabel" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <asp:GridView ID="UnpaidBillGridView" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            
             <Columns>
                <asp:TemplateField HeaderText="SL.NO">
                   <ItemTemplate>
                     <%#Container.DataItemIndex+1 %>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="BillNo" HeaderText="Bill No" SortExpression="BillNo" />
                <asp:BoundField DataField="MoblieNo" HeaderText="Contact No" SortExpression="MoblieNo" />
                <asp:BoundField DataField="PatientName" HeaderText="Patient Name" SortExpression="PatientName" />
                 <asp:BoundField DataField="BillAmount" HeaderText="Bill Amount" SortExpression="BillAmount" />

            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="unpaidBillPdfGenButton" runat="server" style="font-weight: 700" Text="PDF" Width="64px" OnClick="unpaidBillPdfGenButton_Click" />
&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Total"></asp:Label>
&nbsp;<asp:TextBox ID="unpaidTotalTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <br />
    </div>
</asp:Content>

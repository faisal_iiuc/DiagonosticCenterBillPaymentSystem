﻿using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class UnpaidBillReport : System.Web.UI.Page
    {
    
        DateRange dateRange=new DateRange();
        UnpaidBillManager unpaidBillManager=new UnpaidBillManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulategridView();
        }

        private void PopulategridView()
        {
            List<UnpaidBillView> unpaidBillViews = unpaidBillManager.SearchAllUnpaidBill();
            UnpaidBillGridView.DataSource = unpaidBillViews;
            UnpaidBillGridView.DataBind();
 	
        }

        protected void typeWiseShowButton_Click(object sender, EventArgs e)
        {
            unpaidTotalTextBox.Text=string.Empty;

            dateRange.Date1 = date1TextBox.Text;
            dateRange.Date2=date2TextBox.Text;
            List<UnpaidBillView> unpaidBillViews =unpaidBillManager.SearchUnpaidBill(dateRange);
            double totalAmount = 0;
            if (unpaidBillViews.Count==0)
            {
                messageLabel.Text = "NoT Found Any Unpaid Bill!!!";
                PopulategridView(unpaidBillViews);
                return;
            }
            else
            {
                PopulategridView(unpaidBillViews);
            }
            

            foreach(var amount in unpaidBillViews)
            {
                totalAmount += amount.BillAmount;
            }
            unpaidTotalTextBox.Text = totalAmount.ToString();
            ViewState["unpaidBill"] = unpaidBillViews;
        }

     private void PopulategridView(List<UnpaidBillView> unpaidBillViews)
          {
               UnpaidBillGridView.DataSource=unpaidBillViews;
                   UnpaidBillGridView.DataBind();
 	
         }

     protected void unpaidBillPdfGenButton_Click(object sender, EventArgs e)
     {
         List<UnpaidBillView> unpaidBillViews = (List<UnpaidBillView>)ViewState["unpaidBill"];
         pdfGenerate(unpaidBillViews);
     }
     private void pdfGenerate(List<UnpaidBillView> unpaidBillViews)
     {
         using (StringWriter sW = new StringWriter())
         {
             using (HtmlTextWriter hW = new HtmlTextWriter(sW))
             {
                 StringBuilder sB = new StringBuilder();

                 sB.Append("<h2 align='left'  >Date:" + DateTime.Now + "</h2><br>");
                 sB.Append("<h2 align='left'  >K.F.S.S Diagonostic Center</h2><br>");
                 sB.Append("<h2 align='left'  >Unpaid Bill</h2><br><br><br>");
                 sB.Append("<Table align='Center' Border='1'><tr><th>Sl.No</th><th>Bill No</th><th>Contact No</th><th>Patient Name</th><th>Bill Amount</th></tr>");
                 double total = 0;
                 int index = 0;
                 foreach (var unpaidBill in unpaidBillViews)
                 {
                     index += 1;
                     total += unpaidBill.BillAmount;
                     sB.Append("<tr><td>");
                     sB.Append(index);
                     sB.Append("</td>");
                     sB.Append("<td>");
                     sB.Append(unpaidBill.BillNo);
                     sB.Append("</td>");
                     sB.Append("<td>");
                     sB.Append(unpaidBill.MoblieNo);
                     sB.Append("</td><td>");
                     sB.Append(unpaidBill.PatientName);
                     sB.Append("</td><td>");
                     sB.Append(unpaidBill.BillAmount);
                     sB.Append("</td></tr>");

                 }

                 sB.Append("</Table></br>");
                 sB.Append("Total:");
                 sB.Append(total);

                 StringReader sR = new StringReader(sB.ToString());
                 Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                 HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
                 PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                 pdfDoc.Open();
                 htmlParser.Parse(sR);
                 pdfDoc.Close();

                 Response.ContentType = "application/pdf";
                 Response.AddHeader("Content-disposition", "attachment;filename=UnpaidBillReport_" + DateTime.Now + ".pdf");
                 Response.Cache.SetCacheability(HttpCacheability.NoCache);
                 Response.Write(pdfDoc);
                 Response.End();


             }


         }

     }

    }
}
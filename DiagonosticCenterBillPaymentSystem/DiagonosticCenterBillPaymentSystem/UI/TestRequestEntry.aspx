﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="TestRequestEntry.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.TestRequestEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../jquery-ui.css" rel="stylesheet" />
    <fieldset style="width: 35%">
            <legend><strong>Test Request Entry </strong> </legend>
            <strong>
            <br/>
            </strong>
            <center>
                <table>
                    <tr>
                        <td>
                             <strong>
                             <asp:Label ID="Label1" runat="server" Text="Name Of The Patient"></asp:Label>
                             </strong>
                        </td>
                        
                        <td>
                            <strong>
                            <asp:TextBox ID="patientNameTextBox" runat="server"></asp:TextBox>
                            </strong>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <strong>
                            <asp:Label ID="Label2" runat="server" Text="Date Of Birth"></asp:Label>
                            </strong>
                        </td>
                        
                        <td>
                            <strong>
                            <asp:TextBox ID="patientDobTextBox" runat="server"></asp:TextBox>
                            </strong>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <strong>
                            <asp:Label ID="Label3" runat="server" Text="Mobile No"></asp:Label>
                            </strong>
                        </td>
                        
                        <td>
                            <strong>
                            <asp:TextBox ID="patientMobNoTextBox" runat="server"></asp:TextBox>
                            </strong>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <strong>
                            <asp:Label ID="Label4" runat="server" Text="Select Test"></asp:Label>
                            </strong>
                        </td>
                        
                        <td>
                            <strong>
                            <asp:DropDownList ID="patientTestSelectDropDownList" AutoPostBack="true" runat="server" OnSelectedIndexChanged="patientTestSelectDropDownList_SelectedIndexChanged">
                             </asp:DropDownList>
                            </strong>
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td>
                                <strong>
                                <asp:Label ID="Label5" runat="server" Text="Fee"></asp:Label>
                            <asp:TextBox ID="patientTestFeeTextBox" runat="server" Width="79px" Enabled="False" OnLoad="patientTestSelectDropDownList_SelectedIndexChanged"></asp:TextBox>      
                                </strong>      
                        </td>
                    </tr>
                    
                     <tr>
                        <td></td>
                        <td>
                             <strong>
                             <asp:Button ID="addTestButton" runat="server" style="margin-left: 32px" Text="Add" Width="74px" OnClick="addTestButton_Click" />
                             </strong>
                        </td>
                    </tr>

                </table>
            </center>
            <strong>
        <br />
<asp:GridView ID="requestGridView" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="372px">
    <Columns>
                <asp:TemplateField HeaderText="SL.NO">
                   <ItemTemplate>
                     <%#Container.DataItemIndex+1 %>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TestName" HeaderText="TestName" SortExpression="TestName" />
                <asp:BoundField DataField="TestFee" HeaderText="TestFee" SortExpression="TestFee" />
               
            </Columns>

    <FooterStyle BackColor="White" ForeColor="#000066" />
    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
    <RowStyle ForeColor="#000066" />
    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
    <SortedAscendingCellStyle BackColor="#F1F1F1" />
    <SortedAscendingHeaderStyle BackColor="#007DBB" />
    <SortedDescendingCellStyle BackColor="#CAC9C9" />
    <SortedDescendingHeaderStyle BackColor="#00547E" />
</asp:GridView>
        
        
            </strong>
        
        
        <table >
            <tr>
                <td>
                    <strong>
                    <asp:Label ID="Label6" runat="server" Text="Total"></asp:Label>
                    </strong>
                </td>
                <td>
                    <strong>
                    <asp:TextBox ID="totalFeeTextBox" runat="server" ></asp:TextBox>
                    </strong>
                </td>
            </tr>
            
            <tr>
                
                <td>
                    <strong>
                    <asp:Button ID="testReqSaveAndPdfGenButton" runat="server" Text="Save" Width="73px" OnClick="testReqSaveAndPdfGenButton_Click" />
                    <br />
                    <br />
                    <asp:Label ID="messageLabel" runat="server"></asp:Label>
                    </strong>
                </td>
            </tr>
        </table>





    </fieldset>
    
        

</asp:Content>


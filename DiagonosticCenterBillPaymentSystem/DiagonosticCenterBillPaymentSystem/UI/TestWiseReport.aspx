﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="TestWiseReport.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.TestWiseReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <fieldset style="width: 35%">
            
            <legend>Test wise Report</legend>
            <br/>
            <center> 
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="From Date" style="font-weight: 700"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="date1SearchTextBox" runat="server" Width="119px"></asp:TextBox>
                        </td>
                         
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="To Date" style="font-weight: 700"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="date2SearchTextBox" runat="server"></asp:TextBox>
                        </td>
                         <td>
                            <asp:Button ID="typeWiseShowButton" runat="server" Text="Show" OnClick="typeWiseShowButton_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
             
            </center>
        </fieldset>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="messageLabel" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <br />
        <asp:GridView ID="testWiseReportGridView" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="309px">
            
            <Columns>
                <asp:TemplateField HeaderText="SL.NO">
                   <ItemTemplate>
                     <%#Container.DataItemIndex+1 %>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TestName" HeaderText="Test Name" SortExpression="TestName" />
                <asp:BoundField DataField="TotalNoOfTest" HeaderText="Total Test" SortExpression="TotalNoOfTest" />
                <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" SortExpression="TotalAmount" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
         <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="testWisePdfGenButton" runat="server" style="font-weight: 700" Text="PDF" Width="64px" OnClick="testWisePdfGenButton_Click" />
&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" style="font-weight: 700" Text="Total"></asp:Label>
&nbsp;<asp:TextBox ID="testWiseTotalTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>

﻿using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class TypeWiseReport : System.Web.UI.Page
    {
        TypeWiseReportManager typeWiseReportManager = new TypeWiseReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void typeWiseShowButton_Click(object sender, EventArgs e)
        {
            DateRange dateRange = new DateRange();
            dateRange.Date1 = date1SearchTextBox.Text;
            dateRange.Date2 = date2SearchTextBox.Text;



            List<TypeWiseReportView> typeWiseReportViews = typeWiseReportManager.SearchTypeWiseReport(dateRange);

            
            double total = 0;
            foreach (var calculateTotal in typeWiseReportViews)
            {
                total += calculateTotal.TotalAmount;
            }
            
            typeWiseTotalTextBox.Text = total.ToString();
            ViewState["typeWiseReport"] = typeWiseReportViews;
            PopulatedGridView(typeWiseReportViews);
            
            
        }
       

        private void PopulatedGridView(List<TypeWiseReportView> typeWiseReportViews)
        {
            typeWiseReportGridView.DataSource=typeWiseReportViews;
            typeWiseReportGridView.DataBind();
        }

        private void pdfGenerate(List<TypeWiseReportView> typeWiseReportViews)
        {
        using (StringWriter sW=new StringWriter())
         {
             using (HtmlTextWriter hW=new HtmlTextWriter(sW))
             {
                 StringBuilder sB = new StringBuilder();

                 sB.Append("<h2 align='left'  >Date:" + DateTime.Now + "</h2><br>");
                 sB.Append("<h2 align='left'  >K.F.S.S Diagonostic Center</h2><br>");
                 sB.Append("<h2 align='left'  >TypeWiseReport</h2><br><br><br>");
                 sB.Append("<Table align='Center' Border='1'><tr><th>Sl.No</th><th>TypeName</th><th>No Of Test</th><th>Total Amount</th></tr>");
                 double total=0;
                 int index = 0;
                 foreach (var typeWiseReport in typeWiseReportViews)
                 {
                     index +=1;
                     total+=typeWiseReport.TotalAmount;
                     sB.Append("<tr><td>"); 
                        sB.Append (index );
                          sB.Append ("</td>");
                          sB.Append("<td>");
                          sB.Append(typeWiseReport.TestTypeName);
                          sB.Append("</td><td>"); 
                     sB.Append(typeWiseReport.TotalNoOfTest);
                      sB.Append ("</td><td>");
                      sB.Append(typeWiseReport.TotalAmount);
                      sB.Append("</td></tr>");

                 }

                 sB.Append("</Table></br>");
                 sB.Append("Total:");
                 sB.Append(total);

                 StringReader sR=new StringReader(sB.ToString());
                 Document pdfDoc=new Document(PageSize.A4,10f,10f,10f,0f);
                 HTMLWorker htmlParser=new HTMLWorker(pdfDoc);
                 PdfWriter writer= PdfWriter.GetInstance(pdfDoc,Response.OutputStream);
                 pdfDoc.Open();
                 htmlParser.Parse(sR);
                 pdfDoc.Close();

                 Response.ContentType="application/pdf";
                 Response.AddHeader("Content-disposition", "attachment;filename=TestWiseReport_" + DateTime.Now + ".pdf");
                 Response.Cache.SetCacheability(HttpCacheability.NoCache);
                 Response.Write(pdfDoc);
                 Response.End();

                 
             }

            
         }
            
        }

        protected void typeWisePdfGenButton_Click(object sender, EventArgs e)
        {
            List<TypeWiseReportView> typeWiseReportViews = (List<TypeWiseReportView>)ViewState["typeWiseReport"];
            pdfGenerate(typeWiseReportViews);
        }
    }
    }

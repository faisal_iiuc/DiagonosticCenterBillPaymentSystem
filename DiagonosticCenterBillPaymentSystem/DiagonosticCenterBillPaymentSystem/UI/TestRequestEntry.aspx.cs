﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class TestRequestEntry : System.Web.UI.Page
    {
        private SystemManager aSystemManager = new SystemManager();
        TestSetupManager aTestSetUpManager = new TestSetupManager();
        PatientManager apatientManager = new PatientManager();
        RequestManager requestManager = new RequestManager();
        protected void testReqSaveAndPdfGenButton_Click(object sender, EventArgs e)
        {
            Patient patient = new Patient();
            patient.PatientName = patientNameTextBox.Text;
            patient.MoblieNo = patientMobNoTextBox.Text;
            patient.DateofBirth = patientDobTextBox.Text;
            patient.Payment = Convert.ToDouble(totalFeeTextBox.Text);
           List<PatientTestRequest> patientTests = (List<PatientTestRequest>)ViewState["PatientTestRequest"];
           List<PatientWithTestVM> patientWithTestVMList = (List<PatientWithTestVM>)ViewState["Request"];
            patient= apatientManager.SavePatient(patient);


            if(patient.PatientId!=0)
            {
                if (requestManager.SaveRequest(patientTests, patient.PatientId))
                {
                    messageLabel.Text = "Save!!!";
                    pdfGenerate(patientWithTestVMList, patient.BillNo);
                }
                else
                    messageLabel.Text = "Not Save!!!";
                 

            }
            else
            {
                messageLabel.Text = "Not Save!!!";
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                List<Test> tests = aSystemManager.GetAllTests();


                patientTestSelectDropDownList.DataSource = tests;

                patientTestSelectDropDownList.DataTextField = "TestName";

                patientTestSelectDropDownList.DataValueField = "TestId";

                patientTestSelectDropDownList.DataBind();

               
            }

        }

        protected void addTestButton_Click(object sender, EventArgs e)
        {
            
            PatientTestRequest patientTestRequest = new PatientTestRequest();
            patientTestRequest.TestId = Convert.ToInt32(patientTestSelectDropDownList.SelectedValue);
            patientTestRequest.TestFee = Convert.ToDouble(patientTestFeeTextBox.Text);
            PatientWithTestVM patientWithTestVM=new PatientWithTestVM();
            
            patientWithTestVM.TestName = patientTestSelectDropDownList.SelectedItem.ToString();
            patientWithTestVM.TestFee=Convert.ToDouble(patientTestFeeTextBox.Text);

            if (ViewState["Request"] == null)
            {
                List<PatientTestRequest> patientTests = new List<PatientTestRequest>();
                List<PatientWithTestVM> patientWithTestVMs = new List<PatientWithTestVM>();
                patientWithTestVMs.Add(patientWithTestVM);
                patientTests.Add(patientTestRequest);
                ViewState["PatientTestRequest"] = patientTests;
                ViewState["Request"] = patientWithTestVMs;
                double amount=Convert.ToDouble(patientWithTestVM.TestFee);
                ViewState["amount"] = amount;
            }

            else
            {
                
                List<PatientTestRequest> patientTests = (List<PatientTestRequest>)ViewState["PatientTestRequest"];
                List<PatientWithTestVM> patientWithTestVMs = (List<PatientWithTestVM>)ViewState["Request"];
                patientWithTestVMs.Add(patientWithTestVM);
                patientTests.Add(patientTestRequest);
                ViewState["PatientTestRequest"] = patientTests;
                ViewState["Request"] = patientWithTestVMs;

                double amount = (double)ViewState["amount"];
                amount+=Convert.ToDouble(patientWithTestVM.TestFee);
                ViewState["amount"] = amount; 
            }
            List<PatientWithTestVM> patientWithTestVMList = (List<PatientWithTestVM>)ViewState["Request"];
            requestGridView.DataSource = patientWithTestVMList;
            requestGridView.DataBind();
            totalFeeTextBox.Text = ViewState["amount"].ToString();
        }

        protected void patientTestSelectDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Test aTest = new Test();
            aTest.TestId = Convert.ToInt32(patientTestSelectDropDownList.SelectedValue);
            patientTestFeeTextBox.Text = aTestSetUpManager.GetFee(aTest).ToString();


        }


        private void pdfGenerate(List<PatientWithTestVM> testRequests, string patientId)
        {
            using (StringWriter sW = new StringWriter())
            {
                using (HtmlTextWriter hW = new HtmlTextWriter(sW))
                {
                    StringBuilder sB = new StringBuilder();

                    sB.Append("<h2 align='left'  >Date:" + DateTime.Now + "</h2><br>");
                    sB.Append("<h2 align='left'  >K.F.S.S Diagonostic Center</h2><br>");
                    sB.Append("<h2 align='left'  >Patient Test Requests</h2><br><br><br>");
                    sB.Append("<h2 align='left'  >Patient BillNo:"+patientId+"</h2><br><br><br>");
                    sB.Append("<Table align='Center' Border='1'><tr><th>Sl.No</th><th>Test</th><th>Test Fee</th></tr>");
                    double total = 0;
                    int index = 0;
                    foreach (var test in testRequests)
                    {
                        index += 1;
                        total += test.TestFee;
                        sB.Append("<tr><td>");
                        sB.Append(index);
                        sB.Append("</td>");
                        sB.Append("<td>");
                        sB.Append(test.TestName);
                        sB.Append("</td><td>");
                        sB.Append(test.TestFee);
                        sB.Append("</td></tr>");

                    }

                    sB.Append("</Table></br>");
                    sB.Append("Total:");
                    sB.Append(total);

                    StringReader sR = new StringReader(sB.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlParser.Parse(sR);
                    pdfDoc.Close();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-disposition", "attachment;filename=TestRequest_" + DateTime.Now + ".pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();


                }


            }

        }


    }
}
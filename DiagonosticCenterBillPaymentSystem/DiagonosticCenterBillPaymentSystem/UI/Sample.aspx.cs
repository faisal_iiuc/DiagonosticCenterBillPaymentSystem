﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiagonosticCenterBillPaymentSystem.BLL;
using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System.Diagnostics;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace DiagonosticCenterBillPaymentSystem.UI
{
    public partial class Sample : System.Web.UI.Page
    {
        SystemManager systemManager = new SystemManager();
        protected void Page_Load(object sender, EventArgs e)
        {
           

           
            //systemManager.SearchUnpaidBill(date1,date2);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //DateRange dateRange = new DateRange();

            ////dateRange.Date1 = "12/10/2015";
            ////dateRange.Date2 = "12/10/2017";


            //systemManager.SearchPatientUsingSerialNo("1");

            //PdfDocument pdf = new PdfDocument();
            //pdf.Info.Title = "My First PDF";
            //PdfPage pdfPage = pdf.AddPage();
            //XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            //XFont font = new XFont("Verdana", 20, XFontStyle.Bold);
            //graph.DrawString("This is my first PDF document", font, XBrushes.Black, new XRect(0, 0, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.TopLeft);


            ////string pdfFilename = "firstpage.pdf";
            ////pdf.Save(pdfFilenam;

            int serialNo=1;
            
         using (StringWriter sW=new StringWriter())
         {
             using (HtmlTextWriter hW=new HtmlTextWriter(sW))
             {
                 StringBuilder sB = new StringBuilder();

                 sB.Append("<h2 align='left' >SerialNo:"+ serialNo +"</h2> <h2 align='right'>Date:" + DateTime.Now+"</h2>");
                 sB.Append("<h2>Table</h2>");

                 
                 StringReader sR=new StringReader(sB.ToString());
                 Document pdfDoc=new Document(PageSize.A4,10f,10f,10f,0f);
                 HTMLWorker htmlParser=new HTMLWorker(pdfDoc);
                 PdfWriter writer= PdfWriter.GetInstance(pdfDoc,Response.OutputStream);
                 pdfDoc.Open();
                 htmlParser.Parse(sR);
                 pdfDoc.Close();

                 Response.ContentType="application/pdf";
                 Response.AddHeader("Content-disposition","attachment;filename=Invoice_"+serialNo+".pdf");
                 Response.Cache.SetCacheability(HttpCacheability.NoCache);
                 Response.Write(pdfDoc);
                 Response.End();

                 
             }

            
         }
            
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/SiteIndex.Master" AutoEventWireup="true" CodeBehind="TestSetUp.aspx.cs" Inherits="DiagonosticCenterBillPaymentSystem.UI.TestSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="font-weight: 700">
        <fieldset style="width: 35%">
            <legend>Test Setup </legend>
            <br />
            <center>
              <%--  <asp:Panel ID="Panel1" runat="server" BorderWidth="1px" BorderStyle="Outset" >--%>
                    
                    <table >
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Test Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="testNameTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Fee"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="testFeeTextBox" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="BDT" Font-Bold="True" Font-Italic="False"></asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Test Type"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="testTypeDropDownList" runat="server">
                                    <asp:ListItem>Blood</asp:ListItem>
                                    <asp:ListItem>X-Ray</asp:ListItem>
                                    <asp:ListItem>USG</asp:ListItem>
                                    <asp:ListItem>ECG</asp:ListItem>
                                    <asp:ListItem>Echo</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="saveTestSetupButton" runat="server" Text="Save" OnClick="saveTestSetupButton_Click" />
                    <br />
                    <asp:Label ID="messageLabel" runat="server"></asp:Label>
                    <br />
                    <asp:GridView ID="testSetupGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Comic Sans MS" ForeColor="#333333" GridLines="None" Width="487px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField HeaderText="SL.NO">
                   <ItemTemplate>
                     <%#Container.DataItemIndex+1 %>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TSetupName" HeaderText="Test Name" SortExpression="TestName" />
                <asp:BoundField DataField="TSetupFee" HeaderText="Fee" SortExpression="Fee" />
                <asp:BoundField DataField="TTypeName" HeaderText="Type" SortExpression="TestType" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
                    <br />
             <%--   </asp:Panel>--%>
            </center>
        </fieldset>
        
    </div>
</asp:Content>

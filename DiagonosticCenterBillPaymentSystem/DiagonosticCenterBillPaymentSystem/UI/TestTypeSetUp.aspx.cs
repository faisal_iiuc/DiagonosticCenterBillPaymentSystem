﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiagonosticCenterBillPaymentSystem.BLL;


namespace DiagonosticCenterBillPaymentSystem.UI
{
    
    public partial class TestTypeSetUp : System.Web.UI.Page
    {
        TestTypeManager aTestTypeManager = new TestTypeManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateTestTypeGridView();
        }
        private void PopulateTestTypeGridView()
        {
            List<TestType> testTypes = aTestTypeManager.GetAllTestTypes();
            testTypeGridView.DataSource = testTypes;
            testTypeGridView.DataBind();
        }
        protected void saveTypeButton_Click(object sender, EventArgs e)
        {
            TestType aTestType = new TestType();

            aTestType.TypeName = typeNameTextBox.Text;

            typeNameTextBox.Text = "";
            messageLabel.Text = aTestTypeManager.Save(aTestType);
            PopulateTestTypeGridView();
        }
    }
}
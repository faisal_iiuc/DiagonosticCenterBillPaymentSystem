﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using DiagonosticCenterBillPaymentSystem.DAL.Model;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class TestSetupGateway:Gateway
    {
        public int Save(Test testSetup)
        {
            Query = "INSERT INTO Test VALUES('"+testSetup.TestName+"', '"+testSetup.TestFee+"', '"+testSetup.TypeId+"')";
            Connection.Open();
            Command = new SqlCommand(Query,Connection);
            
            int rowAffected = Command.ExecuteNonQuery();

            Connection.Close();

            return rowAffected;
        }

        public bool DoesTestNameExist(Test testSetup)
        {
            Query = "SELECT * FROM Test WHERE TestName ='" + testSetup.TestName+ "'";
            Command = new SqlCommand(Query, Connection);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool hasRow = false;
            if (Reader.HasRows)
            {
                hasRow = true;
            }

            Reader.Close();
            Connection.Close();
            return hasRow;
        }

        public List<TestSetupVM> GetTestSetupWithTestNameType()
        {
            Query = "SELECT * FROM GetTestSetupWithTestNameType ORDER BY TSetupName";
            Connection.Open();

            Command = new SqlCommand(Query, Connection);

            Reader = Command.ExecuteReader();

            List<TestSetupVM> testSetupList = new List<TestSetupVM>();

            while (Reader.Read())
            {
                TestSetupVM aTestSetupVM = new TestSetupVM();

                aTestSetupVM.TSetupID = (int)Reader["TSetupID"];
                aTestSetupVM.TSetupName = Reader["TSetupName"].ToString();
                aTestSetupVM.TSetupFee = Convert.ToDouble(Reader["TSetupFee"]);
                aTestSetupVM.TTypeName = Reader["TTypeName"].ToString();
                aTestSetupVM.TTypeID = (int)Reader["TTypeID"];
                
                testSetupList.Add(aTestSetupVM);
            }

            Reader.Close();
            Connection.Close();

           return testSetupList;
        }

        public double GetFee(Test test)
        {
            Query = "SELECT TestFee FROM Test WHERE TestId='"+test.TestId+"' ";
            Connection.Open();

            Command = new SqlCommand(Query, Connection);

            Reader = Command.ExecuteReader();

            double Fee=0;

            while (Reader.Read())
            {


                Fee = Convert.ToDouble(Reader["TestFee"]);
            }

            Reader.Close();
            Connection.Close();

            return Fee;
        }
        public List<Test> GetAllTest()
        {
            Query = "SELECT * FROM Test";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            List<Test> tests = new List<Test>();

            while (Reader.Read())
            {
                Test test = new Test();

                test.TestId = (int)Reader["TestId"];
                test.TestName = Reader["TestName"].ToString();
                tests.Add(test);
            }
            Reader.Close();
            Connection.Close();

            return tests;

        }
        

        
    }
}
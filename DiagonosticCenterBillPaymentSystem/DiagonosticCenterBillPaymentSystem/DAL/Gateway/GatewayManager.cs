﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using DiagonosticCenterBillPaymentSystem.DAL.Gateway;
using DiagonosticCenterBillPaymentSystem.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class GatewayManager:Gateway
    {
        
      
      
        public List<UnpaidBillView> SearchUnpaidBill(DateRange date)
        {
            Query = "SELECT * FROM Patient WHERE Payment=((0)) and CAST(DueDate AS date) BETWEEN '" + date.Date1 + "' and '" + date.Date2 + "'  ";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            List<UnpaidBillView> unpaidBillViews = new List<UnpaidBillView>();

            while (Reader.Read())
            {
                UnpaidBillView unpaidBillView = new UnpaidBillView();

               // unpaidBillView.BillNo = (int)Reader["PatientId"];
                unpaidBillView.BillAmount = Convert.ToDouble(Reader["Amount"]);
                unpaidBillView.PatientName = Reader["PatientName"].ToString();
                unpaidBillView.MoblieNo = Reader["MobileNo"].ToString();

                unpaidBillViews.Add(unpaidBillView);
            }
            Reader.Close();
            Connection.Close();

            return unpaidBillViews;
          
        }

       public  TypeWiseReportView SearchTypeWiseReport(TestType type,DateRange date )
        {
            
              
          
                Query = " SELECT Request.TestFee FROM Request JOIN Test ON Request.TestId=Test.TestId JOIN Patient ON Request.PatientId=Patient.PatientId WHERE TEST.TypeId='"+type.TypeId+"' and CAST(Patient.DueDate AS date) BETWEEN '"+date.Date1+"' and '"+date.Date2+"' ";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            TypeWiseReportView typeWiseReportView = new TypeWiseReportView();
            typeWiseReportView.TestTypeName = type.TypeName;
               while (Reader.Read())
               {
                   typeWiseReportView.TotalNoOfTest += 1;
                   typeWiseReportView.TotalAmount += Convert.ToDouble(Reader["TestFee"]);

                   
               }
               Reader.Close();
               Connection.Close();

               return typeWiseReportView;
          
           
        }

       public List<TestType> SearchType()
       {
           Query = "SELECT * FROM TestType";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Reader = Command.ExecuteReader();

           List<TestType> testTypes = new List<TestType>();

           while (Reader.Read())
           {
               TestType testType = new TestType();

               testType.TypeId = (int)Reader["TypeId"];
               testType.TypeName = Reader["TypeName"].ToString();
               testTypes.Add(testType);
           }
           Reader.Close();
           Connection.Close();

           return testTypes;
          
       }




       public List<Test> SearchTest()
       {
           Query = "SELECT * FROM Test";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Reader = Command.ExecuteReader();

           List<Test> tests = new List<Test>();

           while (Reader.Read())
           {
               Test test = new Test();

               test.TestId = (int)Reader["TestId"];
               test.TestName = Reader["TestName"].ToString();
               tests.Add(test);
           }
           Reader.Close();
           Connection.Close();

           return tests;
          
       }

       public TestWiseReportView SearchTestWiseReport(Test test, DateRange dateRange)
       {
           Query = " SELECT Request.TestFee FROM Request JOIN Patient ON Request.PatientId=Patient.PatientId  WHERE Request.TestId='" + test.TestId + "' and CAST(Patient.DueDate AS date) BETWEEN '" + dateRange.Date1 + "' and '" + dateRange.Date2 + "' ";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Reader = Command.ExecuteReader();

           TestWiseReportView testWiseReportView = new TestWiseReportView();
           testWiseReportView.TestName = test.TestName;
           
           
           while (Reader.Read())
           {
               testWiseReportView.TotalNoOfTest += 1;
               testWiseReportView.TotalAmount += Convert.ToDouble(Reader["TestFee"]);


           }
           Reader.Close();
           Connection.Close();

           return testWiseReportView;
          
           
           
       }

       public PaymentView SearchPatientUsingMobile(string mobileNo)
       {
           Query = " SELECT * FROM Patient Where MobileNo='"+mobileNo+"' ";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Reader = Command.ExecuteReader();
           PaymentView aPaymentView = new PaymentView();
           while(Reader.Read())
           {
               aPaymentView.Amount = Convert.ToDouble(Reader["Amount"]);
               aPaymentView.DueDate = Convert.ToString(Reader["DueDate"]);
           }

           return aPaymentView;
       }

      public PaymentView SearchPatientUsingSerialNo(string serialNo)
       {
           Query = " SELECT * FROM Patient Where PatientId='" + serialNo + "' ";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Reader = Command.ExecuteReader();
           PaymentView aPaymentView = new PaymentView();
           while (Reader.Read())
           {
               aPaymentView.PatientId = (int) Reader["PatientId"];
               aPaymentView.Amount = Convert.ToDouble(Reader["Amount"]);
               aPaymentView.DueDate = Convert.ToString(Reader["DueDate"]);
           }

           return aPaymentView;
       }

      public int FindExistMobileNo(string mobileNo)
      {
          Query = "Select * FROM Patient Where MobileNo='" + mobileNo + "'";
          Command = new SqlCommand(Query, Connection);
          Connection.Open();
          Reader = Command.ExecuteReader();

          if (Reader.HasRows)
          {
              return (int)Reader["PatientId"];
          }
          return 0;
      }

      public bool SavePatient(Patient patient)
      {
          Query = "INSER INTO Patient VALUES('"+patient.PatientName+"','"+patient.MoblieNo+"','"+patient.DateofBirth+"','"+patient.DueDate+"','"+patient.Payment+"','((patient.PaymentStatus'))";
          Command = new SqlCommand(Query, Connection);
          Connection.Open();
          Command.ExecuteNonQuery();
          Connection.Open();
          return true;
      }

      public bool UpdatePatient(Patient patient)
      {
          Query = "UPDATE  Patient SET VALUES('" + patient.PatientName + "','" + patient.MoblieNo + "','" + patient.DateofBirth + "','" + patient.DueDate + "','" + patient.Payment + "','" + patient.PaymentStatus + "') WHERE PatientId="+patient.PatientId+"";
          Command = new SqlCommand(Query, Connection);
          Connection.Open();
          Command.ExecuteNonQuery();
          Connection.Open();
          return true;
      }

      public bool SaveRequest(Request request)
      {
          Query = "INSERT INTO Request VALUES('" + request.TestId + "','" + request.PatientId+"','" + request.TestFee + "'";
          Command = new SqlCommand(Query, Connection);
          Connection.Open();
          Command.ExecuteNonQuery();
          Connection.Open();
          return true;
      }

      
    }
}
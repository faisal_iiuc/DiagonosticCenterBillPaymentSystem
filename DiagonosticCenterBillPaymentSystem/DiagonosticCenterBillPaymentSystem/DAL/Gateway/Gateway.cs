﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class Gateway
    {
     
       
        private string ConnectionString =  WebConfigurationManager.ConnectionStrings["LibraryDBConnectionString"].ConnectionString;

        public SqlConnection Connection { get; set; }

        public SqlDataReader Reader { get; set; }

        public SqlCommand Command { get; set; }

        public string Query { get; set; }


        public Gateway()
        {
            Connection = new SqlConnection(ConnectionString);

        }

    }
}
    

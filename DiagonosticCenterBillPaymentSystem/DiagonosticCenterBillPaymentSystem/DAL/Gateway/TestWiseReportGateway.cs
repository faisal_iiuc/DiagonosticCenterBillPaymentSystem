﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class TestWiseReportGateway:Gateway
    {
        public TestWiseReportView SearchTestWiseReport(Test test, DateRange dateRange)
        {
            Query = " SELECT Request.TestFee FROM Request JOIN Patient ON Request.PatientId=Patient.PatientId  WHERE Request.TestId='" + test.TestId + "' and CAST(Patient.DueDate AS date) BETWEEN '" + dateRange.Date1 + "' and '" + dateRange.Date2 + "' ";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            TestWiseReportView testWiseReportView = new TestWiseReportView();
            testWiseReportView.TestName = test.TestName;


            while (Reader.Read())
            {
                testWiseReportView.TotalNoOfTest += 1;
                testWiseReportView.TotalAmount += Convert.ToDouble(Reader["TestFee"]);


            }
            Reader.Close();
            Connection.Close();

            return testWiseReportView;



        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DiagonosticCenterBillPaymentSystem.DAL.Model;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class TestTypeGateway:Gateway
    {
        private int rowAffected;

        public int Save(TestType testType)
        {
            Query = "INSERT INTO TestType VALUES('"+testType.TypeName+"')";
            Command = new SqlCommand(Query, Connection);

            Connection.Open();

            rowAffected = Command.ExecuteNonQuery();
            Connection.Close();
            return rowAffected;
        }

        public bool DoesTestNameExist(TestType testType)
        {
            Query = "SELECT * FROM TestType WHERE TypeName ='"+testType.TypeName+"'";
            Command = new SqlCommand(Query, Connection);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool hasRow = false;
            if (Reader.HasRows)
            {
                hasRow = true;
            }

            Reader.Close();
            Connection.Close();
            return hasRow;
        }

        public List<TestType> GetAllTestTypes()
        {
            Query = "SELECT * FROM TestType ORDER BY TypeName";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<TestType> testTypes= new List<TestType>();

            while (Reader.Read())
            {
                TestType atestType = new TestType();
                atestType.TypeId = (int)Reader["TypeId"];
                atestType.TypeName =Reader["TypeName"].ToString();
                testTypes.Add(atestType);
            }
            Reader.Close();
            Connection.Close();
            return testTypes;
        }
    }
}
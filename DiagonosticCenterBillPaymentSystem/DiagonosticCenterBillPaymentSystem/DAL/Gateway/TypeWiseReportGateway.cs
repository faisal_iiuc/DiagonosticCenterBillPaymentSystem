﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class TypeWiseReportGateway:Gateway
    {
        public TypeWiseReportView SearchTypeWiseReport(TestType type, DateRange date)
        {



            Query = " SELECT Request.TestFee FROM Request JOIN Test ON Request.TestId=Test.TestId JOIN Patient ON Request.PatientId=Patient.PatientId WHERE TEST.TypeId='" + type.TypeId + "' and CAST(Patient.DueDate AS date) BETWEEN '" + date.Date1 + "' and '" + date.Date2 + "' ";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            TypeWiseReportView typeWiseReportView = new TypeWiseReportView();
            typeWiseReportView.TestTypeName = type.TypeName;
            while (Reader.Read())
            {
                typeWiseReportView.TotalNoOfTest += 1;
                typeWiseReportView.TotalAmount += Convert.ToDouble(Reader["TestFee"]);


            }
            Reader.Close();
            Connection.Close();

            return typeWiseReportView;


        }
    }
}
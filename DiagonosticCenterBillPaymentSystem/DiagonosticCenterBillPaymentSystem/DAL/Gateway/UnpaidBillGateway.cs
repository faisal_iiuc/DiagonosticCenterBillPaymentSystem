﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class UnpaidBillGateway:Gateway
    {
        public List<UnpaidBillView> SearchUnpaidBill(DateRange date)
        {
            Query = "SELECT * FROM Patient WHERE Payment=((0)) and CAST(DueDate AS date) BETWEEN '" + date.Date1 + "' and '" + date.Date2 + "'  ";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            List<UnpaidBillView> unpaidBillViews = new List<UnpaidBillView>();

            while (Reader.Read())
            {
                UnpaidBillView unpaidBillView = new UnpaidBillView();

                unpaidBillView.BillNo = Reader["SerialNo"].ToString();
                unpaidBillView.BillAmount = Convert.ToDouble(Reader["Amount"]);
                unpaidBillView.PatientName = Reader["PatientName"].ToString();
                unpaidBillView.MoblieNo = Reader["MobileNo"].ToString();

                unpaidBillViews.Add(unpaidBillView);
            }
            Reader.Close();
            Connection.Close();

            return unpaidBillViews;

        }

        public List<UnpaidBillView> SearchAllUnpaidBill()
        {

            Query = "SELECT * FROM Patient WHERE Payment=((0)) ";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();

            List<UnpaidBillView> unpaidBillViews = new List<UnpaidBillView>();

            while (Reader.Read())
            {
                UnpaidBillView unpaidBillView = new UnpaidBillView();

                unpaidBillView.BillNo = Reader["SerialNo"].ToString();
                unpaidBillView.BillAmount = Convert.ToDouble(Reader["Amount"]);
                unpaidBillView.PatientName = Reader["PatientName"].ToString();
                unpaidBillView.MoblieNo = Reader["MobileNo"].ToString();

                unpaidBillViews.Add(unpaidBillView);
            }
            Reader.Close();
            Connection.Close();

            return unpaidBillViews;
        }
    }
}
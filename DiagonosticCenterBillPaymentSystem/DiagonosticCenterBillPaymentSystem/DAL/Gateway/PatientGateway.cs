﻿using DiagonosticCenterBillPaymentSystem.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Gateway
{
    public class PatientGateway:Gateway
    {
        public Patient SearchPatientUsingMobile(string mobileNo)
        {
            Query = " SELECT * FROM Patient Where MobileNo='" + mobileNo + "' ";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            Patient aPatient = new Patient();
            while (Reader.Read())
            {
                aPatient.Payment = Convert.ToDouble(Reader["Amount"]);
                aPatient.DueDate = Convert.ToString(Reader["DueDate"]);
                aPatient.MoblieNo = Reader["MobileNo"].ToString();
                aPatient.PatientId = (int)Reader["PatientId"];
                aPatient.PatientName = Reader["PatientName"].ToString();
                aPatient.PaymentStatus =Convert.ToInt32(Reader["Payment"]);
            }
            Reader.Read();
            Connection.Close();

            return aPatient;
        }
        
        public Patient UpdatePatient(Patient patient)
        {
            Query = "UPDATE  Patient SET Payment=((1)) WHERE SerialNo='" + patient.BillNo + "'";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.ExecuteNonQuery();
            Connection.Close();
            return patient;
        }

        public string SavePatient(Patient patient)
        {
            int totalPatient = GetTotalpatient();
            string SerialNo=DateTime.Now.Year.ToString()+DateTime.Now.Month.ToString("00")+DateTime.Now.Day.ToString("00")+totalPatient.ToString();
            Query = "INSERT INTO Patient VALUES('" + patient.PatientName + "','" + patient.MoblieNo + "','" + patient.DateofBirth + "','" + patient.DueDate + "','" + patient.Payment + "',((0)),'"+SerialNo+"') ";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.ExecuteNonQuery();
            Connection.Close();
            return SerialNo;
        }

        private int GetTotalpatient()
        {
            int count = 0;
            string Query = "SELECT (COUNT(1)+1) FROM Patient";

            SqlCommand command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = command.ExecuteReader();

            while (Reader.Read())
            {
                count = (int)Reader[0];
            }
            Connection.Close();
            return count;
        }

        
    
       


       public Patient UpdatePatientWithIncrementBill(Patient patient)
       {
           Query = "UPDATE  Patient SET VALUES('" + patient.PatientName + "','" + patient.MoblieNo + "','" + patient.DateofBirth + "','" + patient.DueDate + "','" + patient.Payment + "'+Amount,'" + patient.PaymentStatus + "') WHERE PatientId=" + patient.PatientId + "";
           Command = new SqlCommand(Query, Connection);
           Connection.Open();
           Command.ExecuteNonQuery();
           Connection.Close();
           return patient;
       }

       public Patient SearchPatientUsingSerialNo(string serialNo)
       {
               Query = " SELECT * FROM Patient Where SerialNo='" + serialNo + "' ";
               Command = new SqlCommand(Query, Connection);
               Connection.Open();
               Reader = Command.ExecuteReader();
               Patient aPatient = new Patient();
               while (Reader.Read())
               {
                   aPatient.PatientId = (int)Reader["PatientId"];

                   aPatient.Payment = Convert.ToDouble(Reader["Amount"]);
                   aPatient.DueDate = Convert.ToString(Reader["DueDate"]);
                   aPatient.MoblieNo = Reader["MobileNo"].ToString();
                   aPatient.PatientId = (int)Reader["PatientId"];
                   aPatient.PatientName = Reader["PatientName"].ToString();
                   aPatient.PaymentStatus = Convert.ToInt32(Reader["Payment"]);
               }
               Connection.Close();
               return aPatient;
           }
       }
    }

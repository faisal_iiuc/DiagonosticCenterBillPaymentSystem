﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class Test
    {
        public int TestId { get; set; }

        public string TestName { get; set; }

        public int TypeId { get; set; }

        public double TestFee { get; set; }

    }
}
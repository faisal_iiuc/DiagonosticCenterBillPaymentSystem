﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class Request
    {
        public int PatientId { get; set; }

        public int TestId { get; set; }

        public double TestFee { get; set; }
    }
}
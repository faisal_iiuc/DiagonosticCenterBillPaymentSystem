﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class TestType
    {
        public int TypeId { get; set; }

        public string TypeName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class PaymentView
    {
        public double Amount { get; set; }

        public string DueDate { get; set; }
        public int  PatientId { get; set; }
    }
}
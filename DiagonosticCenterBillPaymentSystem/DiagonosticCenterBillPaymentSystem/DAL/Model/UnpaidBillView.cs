﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    [Serializable]
    public class UnpaidBillView
    {
        public string BillNo { get; set; }
        
        public double BillAmount { get; set; }

        public string PatientName { get; set; }

        public string MoblieNo { get; set; }

        
    }
}
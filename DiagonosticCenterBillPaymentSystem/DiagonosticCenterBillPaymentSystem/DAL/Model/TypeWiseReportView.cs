﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    [Serializable]
    public class TypeWiseReportView
    {
        public string TestTypeName { get; set; }

        public int TotalNoOfTest { get; set; }

        public double TotalAmount { get; set; }

        public TypeWiseReportView()
        {
            TotalNoOfTest = 0;
            TotalAmount = 0;
        }

    }
}
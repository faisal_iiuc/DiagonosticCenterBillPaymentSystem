﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    [Serializable]
    public class TestWiseReportView
    {
        public string TestName { get; set; }

        public int TotalNoOfTest { get; set; }

        public double TotalAmount { get; set; }

        public TestWiseReportView()
        {
            TotalAmount = 0;
            TotalNoOfTest = 0;
        }
    }
}
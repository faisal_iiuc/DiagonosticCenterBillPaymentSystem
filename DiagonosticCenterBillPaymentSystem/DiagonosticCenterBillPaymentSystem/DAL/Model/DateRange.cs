﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class DateRange
    {
        public DateTime date1 { get; set; }

        public DateTime date2 { get; set; }

        public string Date1 { get; set; }

        public string Date2 { get; set; }
    }
}
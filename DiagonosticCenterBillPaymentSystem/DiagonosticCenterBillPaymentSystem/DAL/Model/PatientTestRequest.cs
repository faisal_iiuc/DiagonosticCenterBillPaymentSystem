﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    [Serializable]
    public class PatientTestRequest
    {

       

        public double TestFee { get; set; }

        public bool PaymentStatus { get; set; }

        public int TestId { get; set; }
        public string TestName { get; set; }
        public int PatientId { get; set; }
    }
}
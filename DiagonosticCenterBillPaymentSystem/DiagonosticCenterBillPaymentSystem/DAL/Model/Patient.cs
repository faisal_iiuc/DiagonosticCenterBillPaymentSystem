﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.io;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class Patient
    {
        public int PatientId { get; set; }

        public string PatientName { get; set; }

        public string MoblieNo { get; set; }

        public string DateofBirth { get; set; }

        public double Payment { get; set; }

        public string DueDate { get; set; }

        public int PaymentStatus { get; set; }
        public string BillNo { get; set; }
        public Patient()
        {
            Payment = 0;
        }

    }
}
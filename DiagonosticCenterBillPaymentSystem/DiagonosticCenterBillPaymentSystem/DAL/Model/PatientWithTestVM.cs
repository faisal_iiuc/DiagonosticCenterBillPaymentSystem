﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    [Serializable]
    public class PatientWithTestVM
    {
        public string TestName { get; set; }

        public double TestFee { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiagonosticCenterBillPaymentSystem.DAL.Model
{
    public class TestSetupVM
    {
        public int TSetupID { get; set; }
        public string TSetupName { get; set; }

        public double TSetupFee { get; set; }

        public string TTypeName { get; set; }

        public int TTypeID { get; set; }
    }
}
USE [DiagonosticDB]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 12/06/2016 07:44:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Patient](
	[PatientId] [int] IDENTITY(1,1) NOT NULL,
	[PatientName] [varchar](100) NULL,
	[MobileNo] [varchar](11) NULL,
	[DateOfBirth] [date] NULL,
	[DueDate] [date] NULL,
	[Amount] [decimal](18, 2) NULL,
	[Payment] [bit] NULL,
	[SerialNo] [varchar](50) NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TestType]    Script Date: 12/06/2016 07:44:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TestType](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](100) NULL,
 CONSTRAINT [PK_TestType] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Test]    Script Date: 12/06/2016 07:44:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Test](
	[TestId] [int] IDENTITY(1,1) NOT NULL,
	[TestName] [varchar](100) NULL,
	[TestFee] [decimal](18, 2) NULL,
	[TypeId] [int] NULL,
 CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED 
(
	[TestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[GetTestSetupWithTestNameType]    Script Date: 12/06/2016 07:44:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[GetTestSetupWithTestNameType] AS
SELECT Test.TestId TSetupID,Test.TestName TSetupName,Test.TypeId TTypeID,Test.TestFee TSetupFee,TestType.TypeName TTypeName FROM Test JOIN TestType ON Test.TypeId=TestType.TypeId
GO
/****** Object:  Table [dbo].[Request]    Script Date: 12/06/2016 07:44:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request](
	[PatientRequestId] [int] IDENTITY(1,1) NOT NULL,
	[TestId] [int] NULL,
	[PatientId] [int] NULL,
	[TestFee] [decimal](18, 2) NULL,
	[PaymentStatus] [bit] NULL,
 CONSTRAINT [PK_PatientTests] PRIMARY KEY CLUSTERED 
(
	[PatientRequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Patient_Payment]    Script Date: 12/06/2016 07:44:31 ******/
ALTER TABLE [dbo].[Patient] ADD  CONSTRAINT [DF_Patient_Payment]  DEFAULT ((1)) FOR [Payment]
GO
/****** Object:  ForeignKey [FK_Request_Patient]    Script Date: 12/06/2016 07:44:31 ******/
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_Patient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([PatientId])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_Patient]
GO
/****** Object:  ForeignKey [FK_Request_Test]    Script Date: 12/06/2016 07:44:31 ******/
ALTER TABLE [dbo].[Request]  WITH CHECK ADD  CONSTRAINT [FK_Request_Test] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([TestId])
GO
ALTER TABLE [dbo].[Request] CHECK CONSTRAINT [FK_Request_Test]
GO
/****** Object:  ForeignKey [FK_Test_TestType]    Script Date: 12/06/2016 07:44:31 ******/
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [FK_Test_TestType] FOREIGN KEY([TestId])
REFERENCES [dbo].[TestType] ([TypeId])
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [FK_Test_TestType]
GO
